﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] GameObject deathFX;
    [SerializeField] Transform parent;
    [SerializeField] int scoreValue = 10;
    [SerializeField] int hits = 6;

    ScoreBoard scoreBoard;
    bool isDead = false;

    void Start() {
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void OnParticleCollision(GameObject other) {
        if (!isDead) {
            scoreBoard.AddScore(scoreValue);
            hits--;

            if (hits < 1) {
                KillEnemy();
            }
        }
    }

    private void KillEnemy() {
        isDead = true;

        GameObject fx = Instantiate(deathFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        Destroy(gameObject);
    }
}
