﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructor : MonoBehaviour {

    [SerializeField] float lifespan = 5.0f;

	// Use this for initialization
	void Start () {
        DestroyObject(gameObject, lifespan);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
