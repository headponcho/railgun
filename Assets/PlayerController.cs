﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    [Header("General")]
    [Tooltip("In ms^-1")] [SerializeField] float xSpeed = 25.0f;
    [Tooltip("In m")] [SerializeField] float xRange = 7.5f;
    [Tooltip("In ms^-1")] [SerializeField] float ySpeed = 25.0f;
    [Tooltip("In m")] [SerializeField] float yRange = 3.5f;
    [SerializeField] GameObject[] guns;

    [Header("Position-based Factors")]
    [SerializeField] float positionPitchFactor = -5.0f;
    [SerializeField] float positionYawFactor = 5.0f;

    [Header("Control-based Factors")]
    [SerializeField] float controlPitchFactor = -20.0f;
    [SerializeField] float controlRollFactor = -20.0f;

    float xThrow, yThrow;
    bool isDead = false;

    // Update is called once per frame
    void Update() {
        if (!isDead) {
            UpdatePosition();
            UpdateRotation();
            UpdateFiring();
        }
    }

    // called by string reference
    void OnPlayerDeath() {
        isDead = true;
    }

    private void UpdatePosition() {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");

        float xOffset = xThrow * xSpeed * Time.deltaTime;
        float yOffset = yThrow * ySpeed * Time.deltaTime;

        float xPos = Mathf.Clamp(transform.localPosition.x + xOffset, -xRange, xRange);
        float yPos = Mathf.Clamp(transform.localPosition.y + yOffset, -yRange, yRange);

        transform.localPosition = new Vector3(xPos, yPos, transform.localPosition.z);
    }

    private void UpdateRotation() {
        float pitchFromPosition = transform.localPosition.y * positionPitchFactor;
        float pitchFromControl = yThrow * controlPitchFactor;
        float pitch = pitchFromPosition + pitchFromControl;

        float yawFromPosition = transform.localPosition.x * positionYawFactor;
        float yaw = yawFromPosition;

        float rollFromControl = xThrow * controlRollFactor;
        float roll = rollFromControl;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void UpdateFiring() {
        if (CrossPlatformInputManager.GetButton("Fire")) {
            SetGunsActive(true);
        } else {
            SetGunsActive(false);
        }
    }

    private void SetGunsActive(bool isActive) {
        foreach (GameObject gun in guns) {
            ParticleSystem.EmissionModule emitter = gun.GetComponent<ParticleSystem>().emission;
            emitter.enabled = isActive;
        }
    }
}
