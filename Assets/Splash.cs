﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("LoadGame", 2.0f);
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void LoadGame() {
        SceneManager.LoadScene(1);
    }
}
