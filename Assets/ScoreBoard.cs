﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

    int score = 0;
    Text scoreText;

	// Use this for initialization
	void Start () {
        scoreText = gameObject.GetComponent<Text>();
        scoreText.text = score.ToString();
	}

    public void AddScore(int amount) {
        score += amount;
        scoreText.text = score.ToString();
    }
}
